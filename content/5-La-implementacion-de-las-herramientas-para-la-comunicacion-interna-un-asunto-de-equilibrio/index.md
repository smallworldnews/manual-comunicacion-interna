#La implementación de las herramientas para la comunicación interna: un asunto de equilibrio

Entonces, ¿qué herramienta es la ideal para gestionar la comunicación interna? Como ahora veremos, hay multitud de herramientas digitales y analógicas para ello, pero es esencial mantener este equilibrio:

![](images/equilibrio_5.png)

Si el coste de incorporar una nueva herramienta es muy alto, porque requiere de un gran esfuerzo de asimilación por parte de las personas implicadas, difícilmente cumplirá su papel. Por ello suele ocurrir que las herramientas que se terminan utilizando en la comunicación interna no son las mejores posibles, pero sí las más cercanas a algunas de las personas del colectivo y fácilmente extensibles al resto. La pizarra con una agenda mensual donde cada grupo apunta su reserva de la sala de reuniones es un sistema sencillo e intuitivo. Un calendario colectivo on-line, editable desde cualquier dispositivo, tiene más opciones (por ejemplo, revisar desde casa por la noche la disponibilidad de la sala de reuniones), pero estas opciones deben compensar a medio plazo el esfuerzo colectivo de incorporarlo.

El esfuerzo de implementación se reduce enormemente si una organización aprende a utilizar herramientas colaborativas ocasionales, como Doodle o Riseup Pad. La implementación es más costosa cuando las herramientas son aplicaciones para la gestión de una comunidad digital (como una intranet, por ejemplo):
