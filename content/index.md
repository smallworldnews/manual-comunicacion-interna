# Manual de comunicación interna

En esta guía de Comunicación para la ciudadanía organizada, vas a aprender recursos y herramientas para la comunicación interna.

Uno de los públicos comunicativos más relevantes para cualquier organización es justamente el de las personas que la conforman. Si la comunicación interna no funciona, es difícil que lo haga la comunicación “hacia fuera”. Sin embargo, cuando en una organización hay distintas personas trabajando por departamentos o tareas y falla la comunicación entre ellas, en realidad hablamos de un problema de coordinación más amplio, que excede el terreno de la comunicación. Esto se puede hacer evidente, por ejemplo, en la gestión y uso de los recursos compartidos.

Y, al contrario, una comunicación fluida entre equipos o personas de una misma organización estimula la cooperación entre áreas de trabajo. Por tanto, la salud de la comunicación interna requiere no sólo de herramientas comunicativas, sino también de mecanismos de coordinación.

---
