# Herramientas digitales para la comunicación interna

Aunque el papel de las herramientas tradicionales en la comunicación interna es clave para el buen funcionamiento de la organización, contamos con multitud de herramientas digitales que también pueden mejorar este tipo de comunicación.

Veamos, primero, una forma de estructurar este tipo de herramientas digitales, según las principales funciones que realizan:

![](images/aplicacionesdigitales_4.png)

Algunas de las herramientas digitales están orientadas a trabajar documentos de forma colectiva. Por ejemplo, varias personas redactan un mismo documento, incluso simultáneamente, y pueden visualizar los cambios que el resto aporta. Aquí tienes un listado organizado de herramientas:

## Entornos de trabajo

_**Zoho**_. Grupo de aplicaciones web que permiten crear, compartir y almacenar archivos en línea. También incluye chat, videoconferencias, mail, calendario y herramientas de ofimática en línea.
www.zoho.com

_**Edmodo**_. Plataforma educativa que permite compartir documentos e información y comunicarse en un entorno privado, a modo de red social.
www.edmodo.com

## Generación de ideas en equipo y trabajo en proyectos

_**ProjectPier**_ es una herramienta de software libre que incluye gestión de documentos, tareas, notificaciones por correo, etc. Permite la coordinación de proyectos mediante hitos y fechas límite.
https://www.softaculous.com/apps/projectman/ProjectPier

_**Mindmeister**_. Aplicación para elaborar mapas mentales en línea y de forma colaborativa, útiles hacer lluvias de ideas o estructurar los ejes del trabajo. Permite insertar multimedia, gestionar y asignar tareas y convertirlos en una presentación o en un documento imprimible.
https://www.mindmeister.com/es

_**Doodle**_. Es una solución simple, abierta y gratuita para calendarizar reuniones. Doodle es un sitio web y un servicio que hace que la programación de una reunión sea simple mediante una encuesta. Al invitar a la gente (mediante el envío de una URL o invitándolos directamente desde Doodle), se puede votar la disponibilidad de preferencia y luego enviar recordatorios.
https://doodle.com/es/

_**Symbaloo**_. Tablero virtual para compartir enlaces o recursos web interesantes, perfecto para recopilar fuentes o documentación.
https://www.symbaloo.com/welcome

_**Hyperisland Toolbox.**_ Si se trata de buscar inspiración y romper los esquemas tradicionales, te recomendamos la caja de herramientas de Hyperisland. Se trata de métodos y herramientas que ayudan a los equipos a hacer tareas de manera más creativa y colaborativa.
http://toolbox.hyperisland.com/

_**Padlet.**_ Herramienta para crear murales virtuales de forma colaborativa, en los que se pueden incluir elementos multimedia, vínculos y documentos.
https://es.padlet.com/

_**Stormboard.**_ Herramienta online para hacer lluvias de ideas 2.0 e intercambiar opiniones sobre un tablero virtual. La versión gratuita permite trabajar con grupos de hasta cinco usuarios.
https://www.stormboard.com/

_**Remind.**_ Aplicación de mensajería segura donde los números quedan ocultos. Además, permite enviar adjuntos y clips de voz, y establecer una agenda de tareas con recordatorios.
https://www.remind.com/

_**Hightrack.**_ Gestor de tareas online y descargable para organizar el trabajo, gestionar una agenda de tareas personal y establecer plazos de entrega o cumplimiento.
http://hightrack.me/en/

_**WorkFlowy.**_ Herramienta en línea con la que se puede establecer un flujo de trabajo colaborativo con tareas jerarquizadas de forma muy visual. Los usuarios o invitados a la lista pueden aportar y modificar el flujo según se cumplan objetivos.
https://workflowy.com/

_**Symphonical.**_ Calendario virtual a modo de pizarra en el que se pueden añadir y gestionar tareas a través de notas adhesivas multimedia. Permite la edición colaborativa entre un grupo establecido y enlaza directamente con Google Hangouts para chatear o hacer videoconferencias.
https://www.upwave.io/

## Elaboración colectiva de documentos

_**Pad de riseup**_ Es una herramienta en línea de software libre que permite que varias personas trabajen sobre un mismo documento de texto de forma rápida y sencilla. Cada usuario tiene un nombre y un color con el que trabajar y accede al documento a través de un enlace.
https://pad.riseup.net/

_**Dropbox.**_ El servicio de almacenamiento en línea más utilizado, para guardar todo tipo de archivos. Ofrece la posibilidad de crear carpetas compartidas con otros usuarios y conectarse desde distintos dispositivos mediante apps.
https://www.dropbox.com/

_**Google Drive.**_ Almacenamiento en línea de 15 Gb, para guardar y compartir todo tipo de documentos y carpetas. Disponible como aplicación para móviles y tabletas. Además, permite editar directamente los documentos en línea con Google Docs

## Videollamadas, chats y reuniones virtuales

_**Mumble.**_ Mumble es una aplicación multiplataforma libre de voz sobre IP especializada en la multiconferencia. Sus principales usuarios son jugadores, y es similar a programas privativos como TeamSpeak y Ventrilo.
https://www.mumble.com/

_**Slack**_ es similar a un chat ordenado por canales. Allí podemos invitar a personas al equipo/canal, comunicarnos mediante mensajería directa, compartir archivos en tiempo real, chatear, guardar mensajes, etiquetar conversaciones, crear alertas, programar bots e incluso realizar videollamadas
https://slack.com/intl/es

_**GoToMeeting.**_ Es una herramienta para video conferencias.  En un mercado laboral donde muchas veces las personas no comparten un espacio común, es clave contar con herramientas que faciliten la comunicación virtual a distancia. Una de las ventajas de GoToMeeting que la distingue del resto de los servicios de webconference es que es instantáneo. Sólo es necesario compartir una URL sin necesidad de contar con un código, número de teléfono o pasos de registro.
https://free.gotomeeting.com/

_**Mikogo**_ es una herramienta para compartir pantalla y organizar conferencias en línea. Permite un máximo de 25 participantes en su versión gratuita.
https://www.mikogo.es/

_**Teamviewer**_ permite el contacto con otras personas y además el control remoto del ordenador de nuestro interlocutor o grabar sesiones (ideal para vídeo tutoriales) y, por supuesto, acceder desde casa al ordenador de la oficina.

_**Skype**_ nos permite realizar videollamadas individuales o grupales. Eso sí, es de Microsoft 🙁

_**Google Hangouts o google meet.**_ Aplicación con la que se puede establecer un grupo de chat o videochat (hasta 10 personas) que permite enviar lecciones online a los alumnos o crear un grupo virtual de intercambio de opiniones.

## Compartir y difundir contenidos

_**WordPress.**_ Una de las herramientas de creación de blogs más completas, ya que permite personalizar y adaptar la bitácora a las necesidades de cada usuario.


_**WeTransfer.**_ Una forma sencilla de enviar documentos, especialmente de gran tamaño (hasta 2 Gb), a cualquier usuario a través de un enlace por email. Los archivos no se almacenan, solo se conservan durante unos días y después se borran.
