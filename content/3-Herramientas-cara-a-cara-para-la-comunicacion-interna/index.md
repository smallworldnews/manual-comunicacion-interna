# Herramientas cara a cara para la comunicación interna

Pues bien, una vez tenemos claro el poder de las herramientas cara a cara, veamos un listado de algunas herramientas de comunicación interna basadas en la conversación y el encuentro:

**1. Reuniones Interdepartamentales.** Uno de los clásicos en toda organización, pero no por ello menos importante. Tienen como objetivo conectar el trabajo de las diferentes áreas de la organización, así que al menos presentan dos formatos: a) Reuniones interdepartamentales técnicas, para ir resolviendo la coordinación del trabajo en una campaña compartida. b) Reuniones interdepartamentales estratégicas, para diseñar trabajos futuros y nuevas formas de coordinación que beneficien a ambos departamentos.

**2. Píldoras de posicionamiento colectivo.** Cada cierto tiempo es casi imprescindible una puesta en común de las tareas que realizan cada departamento o persona de la organización. Las píldoras de posicionamiento son microintervenciones en las que cada persona presenta al resto en tan sólo 5 minutos que trabajo están realizando. Tras cada píldora se generan turnos de preguntas y un debate para la búsqueda de confluencias, si es necesario.

**3. El relator rotatorio.** De forma rotativa, cada cierto tiempo (por ejemplo, un mes) una persona explica al resto de la organización qué temas está trabajando y cómo desarolla su trabajo, con el objetivo de comprender lo que hace y buscar confluencias con el resto de las personas de la organización. Esta técnica, además de ayudar a empatizar entre los distintos equipos y potenciar la identificación del grupo, permite prever posibles problemas de coordinación entre departamentos y áreas.

**4. El desayuno de coordinación.** En ocasiones especiales, por ejemplo, cuando se lanza alguna campaña o se inicia un proyecto, se puede realizar un desayuno con todas las personas para comenzar la jornada laboral de forma conjunta, repartiéndose roles (no sólo para ese día, sino para las próximas semanas). El formato sería el desayuno, símbolo del inicio de la jornada, para generar un clima más relajado y positivo.
