#Utilizar correctamente las herramientas digitales

Con todo, las herramientas colaborativas ocasionales de poco sirven sin un uso correcto y un aprendizaje colectivo para afinar su uso. Un ejemplo muy típico es la lista de correo: se usa mucho, pero normalmente sin unas normas básicas que eviten los problemas que implica. Este listado de consejos es un ejemplo para su correcto uso:

##Consejos para un correcto uso de la lista de distribución

1. Escribe sobre temas relacionados con la lista. Si es sobre otro asunto que pudiera interesar, utiliza la abreviatura [OT] Otro Tema

2. Utiliza un asunto descriptivo para tu correo, de tal forma que no tengamos que abrirlo para saber de qué trata. Si contestas a un correo y hablas de otro tema, reescribe el asunto del mensaje.

3. Intenta sintetizar. Las frases breves tendrán más impacto que los largos párrafos, lo cuales, probablemente serán menos leídos.

4. Cuidado con el uso de siglas o nombres no conocidos por todas. Para no dejar fuera a nadie, es importante explicar brevemente el significado de unas siglas o explicar quién es esa institución o persona en la primera aparición del mensaje.

5. No mandes fotografías u archivos pesados como adjunto en el correo. Puedes subir esos archivos y fotos a otros sitios, o incluso a Wetransfer.com, y enviar sólo la dirección para quién lo quiera descargar.

6. No generes debates de tipo “¿Qué día prefieres para vernos?” o “¿Qué diseño te gusta más?”: utiliza los documentos colaborativos tipo Riseup Pad, Doodle, Polls…

7. Se sugiere no utilizar mayúsculas en el texto 🙂
