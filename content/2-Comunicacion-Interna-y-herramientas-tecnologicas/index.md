# Comunicación interna y herramientas tecnológicas

Cuando hoy hablamos de comunicación interna parece que hablamos preferentemente de tecnología, y esto es un problema bastante evidente, porque la tecnología por sí misma difícilmente soluciona las necesidades de comunicación de una organización.

**Observa este mapa de la comunicación interna basada en el uso de la tecnología:**

![](images/mapa_1.png)

Al ubicar en este mapa distintas formas de comunicación interna, observamos que las herramientas tecnológicas tienen más sentido para tareas repetitivas y bien delimitadas, pero no tanto para las excepcionales y difíciles de predecir. Además, cuando se une el formalismo de la comunicación institucional con la tecnología, el resultado puede ser peligroso. Por ejemplo, nos desagrada bastante la idea de un despido a través de una carta institucional, porque entendemos que algo así debe darse en un contexto discursivo: posibilidad de profundizar en los motivos del despido, posibilidad de diálogo y réplica, o sencillamente, el uso de un canal más humano para una decisión tan importante.

Por tanto, para decidir qué herramientas tecnológicas cubren nuestras necesidades de comunicación interna debemos, para empezar, eliminar **ese mito de que cualquier tecnología será mejor sólo por serlo y analizar, al menos, 3 factores distintos:**

- Las necesidades comunicativas que cubre la herramienta tecnológica en el terreno específico donde se va a poner en práctica.

- Los costes de implementar la herramienta tecnológica y de que se utilice correctamente.

- Los costes de los usos indebidos de la herramienta tecnológica.

## ¿Qué herramientas de comunicación interna son más eficaces?

La mitificación de las tecnologías nos puede hacer creer que las herramientas digitales o tecnológicas son un avance frente a las herramientas “de toda la vida”. Algunas investigaciones nos dicen que esto no es así. Aunque pueda parecer que el correo electrónico tiene un gran alcance de difusión, basta con pedir algo cara a cara a 6 personas para igualar la efectividad de un correo electrónico enviado a 200 personas, dice una investigación realizada por Mahdi Roghanizada y Vanessa Bohnsb.

Una reciente investigación en la que participa la Universidad Complutense de Madrid demuestra que durante la comprensión del lenguaje el cerebro muestra una actividad diferente si recibe información visual de los  movimientos faciales del interlocutor: los movimientos oculares o labiales que se producen en la conversación cara a cara no solo adquieren gran valor social, también cerebral.

Además, distintas investigaciones afirman que las personas tienden a sobrevalorar su capacidad de persuasión mediante las comunicaciones escritas y subestimar esa capacidad en los encuentros cara a cara. Los comportamientos que han estudiado muestran bastante falta de realismo, por ejemplo, a la hora de ponerse en la piel del receptor de un email: “ni siquiera se planteaban lo que los destinatarios verían en sus correos electrónicos: un mensaje de poca confianza que pide hacer clic en un enlace sospechoso”, explican. Pues bien, son las pistas no verbales que los solicitantes transmiten durante las interacciones cara a cara las que marcan la diferencia, las que aumentan la eficacia comunicativa. A menudo resulta más cómodo escribir a alguien que acercarse directamente a hablar con esa persona, advierten Roghanizada y Bohnsb, por lo que podríamos estar relegando una herramienta realmente potente, aunque sea la reunión cara a cara de toda la vida.

Y por si alguien tuviera dudas del poder de la conversación, contamos con Sherry Turkle, auténtica activista de esta forma de comunicación. No te pierdas su microcharla en TED titulada: ¿Conectados pero solos?
